<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Auth\AuthentificationException;

class ParticipateInForumTest extends TestCase
{

    /*
    function unauthentificated_users_may_not_add_replies()
    {
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $this->post('/threads/1/replies', []);
    } */


    /** @test */
    function an_authentificated_user_may_participate_in_forum_threads()
    {
        // Given we have an authenticated user
        // And an existing thread
        // When the user adds a reply to the thread
        // Then their reply should be visible on the page.


        

        $this->be($user = factory('App\User')->create() );
        $thread = factory ('App\Thread')->create();
        $reply = factory('App\Reply')->create();

        $this->post($thread->path().'/replies', $reply->toArray());
        $this->get($thread->path())->assertSee($reply->body);
    }

    /** @test */
    function a_reply_requires_a_body()
    {
        $this->withExceptionHandling()->signIn();
        $thread = create('App\Thread');
        $reply = make('App\Reply' , ['body' => null]);

        $this->post($thread->path() . '/replies', $reply->toArray())
        ->assertSessionHasErrors('body');

    }

}
