<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ThreadsTest extends TestCase
{

    protected $thread;

    public function setUp()
    {
        parent::setUp();
        $this->thread = factory('App\Thread')->create();
    }

    /** @test */

    function a_thread_can_make_a_string_path()
    {
        $thread = create('App\Thread');
        $this->assertEquals(
         "/threads/{$thread->channel->slug}/{$thread->id}", $thread->path()   
        );
    }

     /** @test */
     function a_user_can_view_all_threads()
     {
         $response = $this->get('/threads');
         $response->assertSee($this->thread->title);
     }
     /** @test */
     public function a_user_can_read_a_single_thread()
     {
        $thread = factory('App\Thread')->create();
        $response = $this->get('/threads/' . $thread->channel->slug. '/' . $thread->id);
        $response->assertSee($thread->title);
        //keep '/threads/' otherwise the link won't be recognised
     }


     /** @test */
     function a_user_can_read_replies_that_are_associated_with_a_thread()
     {
         // Given we have a thread
         // And that thread includes replies
         // When we visit a thread page
         // Then we would see the replies
         $thread = factory('App\Thread')->create();
         $reply = factory('App\Reply')->create(['thread_id' => $this->thread->id]);
         $response = $this->get('/threads/' . $thread->channel->slug . '/' . $this->thread->id)
         ->assertSee( e($reply->body) );
          //keep '/threads/' otherwise the link won't be recognised
         
     }

     /** @test */
     function a_thread_has_a_creator()
     {
         $thread = factory('App\Thread')->create();
         $this->assertInstanceOf('App\User', $thread->creator);
     }

     /** @test */
     function a_thread_has_replies()
     {
         $thread = factory('App\Thread')->create();
         $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $thread->replies);
     }

     /** @test */
     function a_thread_can_add_a_reply()
     {
         $this->thread->addReply([
             'body' => 'Foobar',
             'user_id' => 1
         ]);

         $this->assertCount(1, $this->thread->replies);

     }

    
}
