<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;

class CreateThreadsTest extends TestCase
{
    /** @test */
    
    function guests_may_not_create_threads()
    {
        $this->withoutExceptionHandling()->expectException('Illuminate\Auth\AuthenticationException');
        //handles exception for unauthenticated users
        //using the same function as below without creating an user
        $thread = make('App\Thread');
   
       $response = $this->post('/threads', $thread->toArray());
    }

   /** @test */
   function guest_cannot_access_the_create_thread_page()
   {
       $this->get('threads/create')
       ->assertRedirect('/login');
       //->assertRedirect('/threads/create');
       //uncomment to make the as proof that the test works
   }

   //put test back when fixed
   function an_authenticated_user_can_create_new_forum_threads()
   {
       // Given we have a signed in user
       // Wehn we hit the endpoint to create a new thread
       // Then when we visit the thread page 
       // We should see the new thread

        $this->be(factory('App\User')->create());
       $thread = factory('App\Thread')->create();
   
       $response = $this->post('/threads', $thread->toArray());
    
       //dd($thread->path());
       //above used to get the path

       dd($thread->path());

       $this->get($response->path())
           ->assertSee($thread->title)
           ->assertSee($thread->body);
   }

   /** @test */
   function a_thread_belongs_to_a_channel()
   {
       $thread = create('App\Thread');
       $this->assertInstanceOf('App\Channel', $thread->channel);
   }

/** @test */
function a_thread_requires_a_title()
{
    $this->publishThread(['title' => null])
        ->assertSessionHasErrors('title');
}
/** @test */
function a_thread_requires_a_body()
{
    $this->publishThread(['body' => null])
        ->assertSessionHasErrors('body');
}
/** @test */
function a_thread_requires_a_valid_channel()
{
    factory('App\Channel', 2)->create();
    $this->publishThread(['channel_id' => null])
        ->assertSessionHasErrors('channel_id');
    $this->publishThread(['channel_id' => 999])
        ->assertSessionHasErrors('channel_id');
}
protected function publishThread($overrides = [])
{
    $this->withExceptionHandling()->signIn();
    $thread = make('App\Thread', $overrides);
    return $this->post('/threads', $thread->toArray());
}
}