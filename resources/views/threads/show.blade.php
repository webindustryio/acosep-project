@extends('layouts.app')

@section('content')

 <!--================================
        START BREADCRUMB AREA
    =================================-->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb">
                        <ul>
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li class="active">
                                <a href="/threads">Forum</a>
                            </li>
                            <li class="active">
                                <a href="#">{{ $thread->title}}</a>
                            </li>
                        </ul>
                    </div>
                    <h2 class="page-title">{{$thread->title}}</h2>
                </div>
                <!-- end /.col-md-12 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>
    <!--================================
        END BREADCRUMB AREA
    =================================-->

    <!--================================
            START DASHBOARD AREA
    =================================-->
    <section class="support_threads_area section--padding2">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="forum_detail_area ">
                        <div class="cardify forum--issue">
                            <div class="title_vote clearfix">
                                <h3>{{$thread->title}}</h3>

                                <div class="vote">
                                    <a href="#">
                                        <span class="lnr lnr-thumbs-up"></span>
                                    </a>
                                    <a href="#">
                                        <span class="lnr lnr-thumbs-down"></span>
                                    </a>
                                </div>
                                <!-- end .vote -->
                            </div>
                            <!-- end .title_vote -->
                            <div class="suppot_query_tag">
                                <a href="#" class="user">
                                    <img class="poster_avatar" src="/storage/{{ $thread->owner->avatar}}" alt="Support Avatar"> {{ $thread->creator->name }} </a>
                                <span> {{ $thread->created_at->diffForHumans() }} in</span>
                                <a href="/threads/{{ $thread->channel->slug }}"><span class="support_tag">{{ $thread->channel->name }}</span></a>
                            </div>
                            <p>{{ $thread->body }}</p>
                        </div>
                        <!-- end .forum_issue -->


                        <div class="forum--replays cardify">
                            <div class="area_title">
                                <h4>3 Answers</h4>
                            </div>
                            <!-- end .area_title -->
                            @foreach ($thread->replies as $reply)
                            @include('threads.reply')
                            </div>
                            <!-- end .forum_single_reply -->
                            @endforeach

                            @if(auth()->check())
                            <div class="comment-form-area">
                                <h4>Leave a comment</h4>
                                <!-- comment reply -->
                                <div class="media comment-form support__comment">
                                    <div class="media-body">  
                         <form method="POST" action="{{ $thread->path() . '/replies'}}" class="comment-reply-form">
                                        {{ csrf_field() }}
                                            <textarea name="body" id="body" class="forum-control" placeholder="Have something to day?"></textarea>
                                            <button class="btn btn--sm btn--round" type="submit">Post Comment</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- comment reply -->
                            </div>
                            @else
                            <p class="text-center">Please <a href="{{ route('login') }}"> sign in </a> to participate in this discussion </p>
</br>
                            @endif
                        </div>
                        <!-- end .forum_replays -->
                    </div>
                    <!-- end .forum_detail_area -->
                </div>
                <!-- end .col-md-8 -->
                
                <div class="col-lg-4">
                    <aside class="sidebar support--sidebar">
                    @if(auth()->check())
                    <a href="/threads/create" class="login_promot">
                            <span class="lnr lnr-bubble"></span>Create a new thread</a>
                    @else
                        <a href="{{ route('login')}}" class="login_promot">
                            <span class="lnr lnr-lock"></span>Login to Ask a Question</a>
                    @endif

                        <div class="sidebar-card card--forum_categories">
                            <div class="card-title">
                                <h4>Forum Categories</h4>
                            </div>
                            <div class="collapsible-content">
                                <ul class="card-content">
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>Wordpress
                                            <span class="item-count">35</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>Support Center
                                            <span class="item-count"> 45</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>General Topics
                                            <span class="item-count">13</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>Pre-Sales
                                            <span class="item-count">08</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>Purchases
                                            <span class="item-count">34</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>Billing
                                            <span class="item-count">78</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>Testimonials
                                            <span class="item-count">26</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- end /.collapsible_content -->
                        </div>
                        <!-- end .card--forum_categories -->

                        <div class="sidebar-card card--top_discussion">
                            <div class="card-title">
                                <h4>Top Discussions</h4>
                            </div>
                            <div class="collapsible-content">
                                <ul class="card-content">
                                    <li>
                                        <a href="#">
                                            <span></span>Commissions Transfer Problem ( 12h )</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span></span>Dashboard not updating ( 9h )</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span></span> AdminPro ( 2d )</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span></span>Bill required after purchase ( 3d )</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span></span>How to install the theme? ( 7d )</a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class=""></span>Contact Form not Working ( 9d )</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- end /.collapsible_content -->
                        </div>
                        <!-- end .card--forum_categories -->
                    </aside>
                    <!-- end .sidebar -->
                </div>
                <!-- end .col-md-4 -->
            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </section>
    <!--================================
            END DASHBOARD AREA
    =================================-->



@endsection