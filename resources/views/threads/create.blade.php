@extends('layouts.app')

@section('content')

<!--================================
        START BREADCRUMB AREA
    =================================-->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb">
                        <ul>
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li>
                                <a href="/threads">Forum</a>
                            </li>
                            <li class="active">
                                <a href="/threads">Create a thread</a>
                            </li>
                        </ul>
                    </div>
                    <h1 class="page-title">Create a thread</h1>
                </div>
                <!-- end /.col-md-12 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>
    <!--================================
        END BREADCRUMB AREA
    =================================-->
 <!--================================
            START DASHBOARD AREA
    =================================-->
<section class="support_threads_area section--padding2">
        <div class="container">
<div class="forum--replays cardify animated fadeIn">

                            @if(auth()->check())
                            <div class="comment-form-area">
                                <!-- comment reply -->
                                <div class="media comment-form support__comment">
                                    <div class="media-body">  
                         <form method="POST" action="/threads" class="comment-reply-form">
                                        {{ csrf_field() }}

 <div class="form-group">
                                <label for="channel_id">Choose a Channel:</label>
                                <select name="channel_id" id="channel_id" class="form-control" required>
                                    <option value="">Choose One...</option>

                                    @foreach (App\Channel::all() as $channel)
                                        <option value="{{ $channel->id }}" {{ old('channel_id') == $channel->id ? 'selected' : '' }}>
                                            {{ $channel->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>


                                        <div class="form-group">
                                            <label for="title">Thread subject</label>
                                            <input type="text" name="title" id="title" required maxlength="60" onkeyup="titleCount(this)"class="text_field" placeholder="Your thread title">
                                            
</div>
                                        <div class="pull-right" id="titleNum" id="titleChar"></div>
                                        <br/>
                                        <label for="title">Thread main message</label>
                                            <textarea name="body" id="body" class="forum-control" onkeyup="bodyCount(this)" required placeholder="Have something to day?"></textarea>
                                            <div class="pull-right" id="bodyNum" id="bodyChar"></div>
                                            <button class="btn btn--sm btn--round" type="submit">Post Comment</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- comment reply -->
                            </div>
                            @else
                            <p class="text-center">Please <a href="{{ route('login') }}"> sign in </a> to participate in this discussion </p>
</br>
                            @endif
                        </div>
                        <!-- end .forum_replays -->
                    </div>
                    <!-- end .forum_detail_area -->
                </div>
                <!-- end .col-md-8 -->
</div>
</div>
</section>
@endsection