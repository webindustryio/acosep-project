@extends('layouts.app')

@section('content')
<!--================================
        START BREADCRUMB AREA
    =================================-->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb">
                        <ul>
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li class="active">
                                <a href="#">Forum</a>
                            </li>
                        </ul>
                    </div>
                    <h1 class="page-title">Discussions</h1>
                </div>
                <!-- end /.col-md-12 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>
    <!--================================
        END BREADCRUMB AREA
    =================================-->
 <!--================================
            START DASHBOARD AREA
    =================================-->
    <section class="support_threads_area section--padding2">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="support_threads">
                        <div class="thread_sort_area clearfix">
                            <div class="sort_options">
                                <ul>
                                    <li class="active">
                                        <a href="#">All</a>
                                    </li>
                                    <li>
                                        <a href="#">Open</a>
                                    </li>
                                    <li>
                                        <a href="#">Resolved</a>
                                    </li>
                                    <li>
                                        <a href="#">Closed</a>
                                    </li>
                                    <li>
                                        <a href="#">Unanswered</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- end .sort_options -->

                            <div class="thread_search_area">
                                <form action="#">
                                    <div class="searc-wrap">
                                        <input type="text" placeholder="Search forum">
                                        <button type="submit" class="search-wrap__btn">
                                            <span class="lnr lnr-magnifier"></span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <!-- end .thread_search_area -->
                        </div>
                        <!-- end .thread_sort_area -->

                        <div class="support_thread_list cardify">
                            @foreach ($threads as $thread)
                            <div class="support_thread--single">
                                <div class="support_thread_info">
                                <a href="{{ $thread->path() }}" class="support_title">{{ $thread->title }}
                                    </a>
                                    <div class="suppot_query_tag">
                                           {!!  substr(strip_tags($thread->body), 1, 150) !!}...
                                       <!-- <span class="support_tag">item discussion</span> -->
                                      <div style="padding-top: 10px">
                                          <div class="row">
                                              <div class="col-lg-12">
                                         <p class="pull-left"><img  class="img-fluid" width="20px" src="/storage/{{ $thread->owner->avatar }}"> <strong>{{ $thread->owner->name }}</strong> said {{ $thread->created_at->diffForHumans() }}
                                         <a href="/threads/{{ $thread->channel->slug }}"><span class="support_tag">{{ $thread->channel->name }}</span></a>
                                         <span class="pull-right"><img class="img-fluid" width="18px" src="/storage/thumb-up.svg"> 20 </span> &nbsp; &nbsp;<img class="img-fluid" width="20px" src="/storage/chat.svg"> 5 </span>
                                         </p>
                                                </div>
                                        </div>
                                    </div> 
                                    </div>
                                    
                                </div>
                                <!-- end .support_thread_info -->
</div>
                            @endforeach
                            <!-- end .support_thread--single -->
                           
                        </div>
                        <!-- end .support_thread_list -->
                    </div>
                    <!-- end .support_threads -->
                </div>
                <!-- end .col-md-8 -->

                <div class="col-lg-4">
                    <aside class="sidebar support--sidebar">
                    @if(auth()->check())
                    <a href="/threads/create" class="login_promot">
                            <span class="lnr lnr-bubble"></span>Create a new thread</a>
                    @else
                        <a href="{{ route('login')}}" class="login_promot">
                            <span class="lnr lnr-lock"></span>Login to Ask a Question</a>
                    @endif
                        <div class="sidebar-card card--forum_categories">
                            <div class="card-title">
                                <h4>Forum Categories</h4>
                            </div>
                            <div class="collapsible-content">
                                <ul class="card-content">
                                @foreach (App\Channel::all() as $channel)
                                @endforeach
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>Wordpress
                                            <span class="item-count">35</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>Support Center
                                            <span class="item-count"> 45</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>General Topics
                                            <span class="item-count">13</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>Pre-Sales
                                            <span class="item-count">08</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>Purchases
                                            <span class="item-count">34</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>Billing
                                            <span class="item-count">78</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="lnr lnr-chevron-right"></span>Testimonials
                                            <span class="item-count">26</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- end /.collapsible_content -->
                        </div>
                        <!-- end .card--forum_categories -->

                        <!-- end .card--forum_categories -->
                    </aside>
                    <!-- end .sidebar -->
                </div>
                <!-- end .col-md-4 -->
            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </section>
    <!--================================
            END DASHBOARD AREA
    =================================-->

@endsection