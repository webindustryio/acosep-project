<div class="forum_single_reply">
                                <div class="reply_avatar">
                                    <img src="/storage/{{ $reply->owner->avatar }}" alt="reply avatar">
                                </div>

                                <div class="reply_content">
                                    <div class="name_vote">
                                        <div class="pull-left">
                                            <h4>{{ $reply->owner->name }} 
                                            </h4>
                                            <p>said {{$reply->created_at->diffForHumans() }}</p>
                                        </div>
                                        <!-- end .pull-left -->

                                        <div class="vote">
                                            <a href="#" class="active">
                                                <span class="lnr lnr-thumbs-up"></span>
                                            </a>
                                            <a href="#" class="">
                                                <span class="lnr lnr-thumbs-down"></span>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- end .vote -->
                                    <p>{{$reply->body}}</p>
                                </div>
                                <!-- end .reply_content -->