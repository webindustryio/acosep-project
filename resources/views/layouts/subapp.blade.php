<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

  
    <link rel="stylesheet" href="../css/animate.css" defer>
    <link rel="stylesheet" href="../css/font-awesome.min.css" defer>
    <link rel="stylesheet" href="../css/fontello.css" defer>
    <link rel="stylesheet" href="../css/jquery-ui.css" defer>
    <link rel="stylesheet" href="../css/lnr-icon.css" defer>
    <link rel="stylesheet" href="../css/owl.carousel.css" defer>
    <link rel="stylesheet" href="../css/slick.css" defer>
    <link rel="stylesheet" href="../css/trumbowyg.min.css" defer>
    <link rel="stylesheet" href="../css/bootstrap/bootstrap.min.css" defer>
    <link rel="stylesheet" href="../css/style.css" defer>

    

</head>
<body class="preload login-page">
    <div id="app">
         <!--================================
        START MENU AREA
    =================================-->
    <!-- start menu-area -->
    <div class="menu-area">
        <!-- start .top-menu-area -->
        <div class="top-menu-area">
            <!-- start .container -->
            <div class="container">
                <!-- start .row -->
                <div class="row">
                    <!-- start .col-md-3 -->
                    <div class="col-lg-3 col-md-3 col-6 v_middle">
                        <div class="logo">
                            <a href="{{ url('/') }}">
                         <img src="storage/settings/March2019/ACOSEP-LOGO.png" alt="ACoSeP Logo" class="img-fluid"> 
                            </a>
                        </div>
                    </div>
                    <!-- end /.col-md-3 -->

                    <!-- start .col-md-5 -->
                    <div class="col-lg-8 offset-lg-1 col-md-9 col-6 v_middle">
                        <!-- start .author-area -->
                        <div class="author-area">
                            @guest    
                            <div style="padding:29px 15px;">
                            <a href="{{ route('login') }}" class="btn btn--sm btn--round inline">{{ __('Login') }}</a>
                            @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="btn btn--sm btn--round btn-secondary inline">{{ __('Register') }}</a>
                    </div>
                            @endif
                        @else
                        <div class="author__notification_area">
                                <ul>
                                    <li class="has_dropdown">
                                        <div class="icon_wrap">
                                            <span class="lnr lnr-alarm"></span>
                                            <span class="notification_count noti">25</span>
                                        </div>

                                        <div class="dropdowns notification--dropdown">

                                            <div class="dropdown_module_header">
                                                <h4>My Notifications</h4>
                                                <a href="notification.html">View All</a>
                                            </div>

                                            <div class="notifications_module">
                                                <div class="notification">
                                                    <div class="notification__info">
                                                        <div class="info_avatar">
                                                            <img src="images/notification_head.png" alt="">
                                                        </div>
                                                        <div class="info">
                                                            <p>
                                                                <span>Anderson</span> added to Favourite
                                                                <a href="#">Mccarther Coffee Shop</a>
                                                            </p>
                                                            <p class="time">Just now</p>
                                                        </div>
                                                    </div>
                                                    <!-- end /.notifications -->

                                                    <div class="notification__icons ">
                                                        <span class="lnr lnr-heart loved noti_icon"></span>
                                                    </div>
                                                    <!-- end /.notifications -->
                                                </div>
                                                <!-- end /.notifications -->

                                                <div class="notification">
                                                    <div class="notification__info">
                                                        <div class="info_avatar">
                                                            <img src="images/notification_head2.png" alt="">
                                                        </div>
                                                        <div class="info">
                                                            <p>
                                                                <span>Michael</span> commented on
                                                                <a href="#">MartPlace Extension Bundle</a>
                                                            </p>
                                                            <p class="time">Just now</p>
                                                        </div>
                                                    </div>
                                                    <!-- end /.notifications -->

                                                    <div class="notification__icons ">
                                                        <span class="lnr lnr-bubble commented noti_icon"></span>
                                                    </div>
                                                    <!-- end /.notifications -->
                                                </div>
                                                <!-- end /.notifications -->

                                                <div class="notification">
                                                    <div class="notification__info">
                                                        <div class="info_avatar">
                                                            <img src="images/notification_head3.png" alt="">
                                                        </div>
                                                        <div class="info">
                                                            <p>
                                                                <span>Khamoka </span>purchased
                                                                <a href="#"> Visibility Manager Subscriptions</a>
                                                            </p>
                                                            <p class="time">Just now</p>
                                                        </div>
                                                    </div>
                                                    <!-- end /.notifications -->

                                                    <div class="notification__icons ">
                                                        <span class="lnr lnr-cart purchased noti_icon"></span>
                                                    </div>
                                                    <!-- end /.notifications -->
                                                </div>
                                                <!-- end /.notifications -->

                                                <div class="notification">
                                                    <div class="notification__info">
                                                        <div class="info_avatar">
                                                            <img src="images/notification_head4.png" alt="">
                                                        </div>
                                                        <div class="info">
                                                            <p>
                                                                <span>Anderson</span> added to Favourite
                                                                <a href="#">Mccarther Coffee Shop</a>
                                                            </p>
                                                            <p class="time">Just now</p>
                                                        </div>
                                                    </div>
                                                    <!-- end /.notifications -->

                                                    <div class="notification__icons ">
                                                        <span class="lnr lnr-star reviewed noti_icon"></span>
                                                    </div>
                                                    <!-- end /.notifications -->
                                                </div>
                                                <!-- end /.notifications -->
                                            </div>
                                            <!-- end /.dropdown -->
                                        </div>
                                    </li>

                                   
                                    <li class="has_dropdown">
                                        <div class="icon_wrap">
                                            <span class="lnr lnr-cart"></span>
                                            <span class="notification_count purch">2</span>
                                        </div>

                                        <div class="dropdowns dropdown--cart">
                                            <div class="cart_area">
                                                <div class="cart_product">
                                                    <div class="product__info">
                                                        <div class="thumbn">
                                                            <img src="images/capro1.jpg" alt="cart product thumbnail">
                                                        </div>

                                                        <div class="info">
                                                            <a class="title" href="single-product.html">Finance and Consulting Business Theme</a>
                                                            <div class="cat">
                                                                <a href="#">
                                                                    <img src="images/catword.png" alt="">Wordpress</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="product__action">
                                                        <a href="#">
                                                            <span class="lnr lnr-trash"></span>
                                                        </a>
                                                        <p>$60</p>
                                                    </div>
                                                </div>
                                                <div class="cart_product">
                                                    <div class="product__info">
                                                        <div class="thumbn">
                                                            <img src="images/capro2.jpg" alt="cart product thumbnail">
                                                        </div>

                                                        <div class="info">
                                                            <a class="title" href="single-product.html">Flounce - Multipurpose OpenCart Theme</a>
                                                            <div class="cat">
                                                                <a href="#">
                                                                    <img src="images/catword.png" alt="">Wordpress</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="product__action">
                                                        <a href="#">
                                                            <span class="lnr lnr-trash"></span>
                                                        </a>
                                                        <p>$60</p>
                                                    </div>
                                                </div>
                                                <div class="total">
                                                    <p>
                                                        <span>Total :</span>$80</p>
                                                </div>
                                                <div class="cart_action">
                                                    <a class="go_cart" href="cart.html">View Cart</a>
                                                    <a class="go_checkout" href="checkout.html">Checkout</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!--start .author__notification_area -->

                            <!--start .author-author__info-->
                         <div class="author-author__info inline has_dropdown">
                                <div class="author__avatar">
                                    <div class="circular--landscape">
                                    <img src="http://webindustry.io/acosep/storage/{{ Auth::user()->avatar }}" alt="user avatar">
                                    </div>
                                </div>
                                <div class="autor__info">
                                    <p class="name">
                                    {{ Auth::user()->name }}
                                    </p>
                                </div>

                                <div class="dropdowns dropdown--author">
                                    <ul>
                                        <li>
                                            <a href="author.html">
                                                <span class="lnr lnr-user"></span>Profile</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                <span class="lnr lnr-exit"></span> {{ __('Logout') }}</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @endguest
                            <!--end /.author-author__info-->
                        </div>
                        <!-- end .author-area -->

                        <!-- author area restructured for mobile -->
                        <div class="mobile_content ">
                            <span class="lnr lnr-user menu_icon"></span>

                            <!-- offcanvas menu -->
                            <div class="offcanvas-menu closed">
                                <span class="lnr lnr-cross close_menu"></span>
                                <div class="author-author__info">
                                    <div class="author__avatar v_middle">
                                        <img src="images/usr_avatar.png" alt="user avatar">
                                    </div>
                                    <div class="autor__info v_middle">
                                        <p class="name">
                                            Jhon Doe
                                        </p>
                                        <p class="ammount">$20.45</p>
                                    </div>
                                </div>
                                <!--end /.author-author__info-->

                                <div class="author__notification_area">
                                    <ul>
                                        <li>
                                            <a href="notification.html">
                                                <div class="icon_wrap">
                                                    <span class="lnr lnr-alarm"></span>
                                                    <span class="notification_count noti">25</span>
                                                </div>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="message.html">
                                                <div class="icon_wrap">
                                                    <span class="lnr lnr-envelope"></span>
                                                    <span class="notification_count msg">6</span>
                                                </div>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="cart.html">
                                                <div class="icon_wrap">
                                                    <span class="lnr lnr-cart"></span>
                                                    <span class="notification_count purch">2</span>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!--start .author__notification_area -->

                                <div class="dropdowns dropdown--author">
                                    <ul>
                                        <li>
                                            <a href="author.html">
                                                <span class="lnr lnr-user"></span>Profile</a>
                                        </li>
                                        <li>
                                            <a href="dashboard.html">
                                                <span class="lnr lnr-home"></span> Dashboard</a>
                                        </li>
                                        <li>
                                            <a href="dashboard-setting.html">
                                                <span class="lnr lnr-cog"></span> Setting</a>
                                        </li>
                                        <li>
                                            <a href="cart.html">
                                                <span class="lnr lnr-cart"></span>Purchases</a>
                                        </li>
                                        <li>
                                            <a href="favourites.html">
                                                <span class="lnr lnr-heart"></span> Favourite</a>
                                        </li>
                                        <li>
                                            <a href="dashboard-add-credit.html">
                                                <span class="lnr lnr-dice"></span>Add Credits</a>
                                        </li>
                                        <li>
                                            <a href="dashboard-statement.html">
                                                <span class="lnr lnr-chart-bars"></span>Sale Statement</a>
                                        </li>
                                        <li>
                                            <a href="dashboard-upload.html">
                                                <span class="lnr lnr-upload"></span>Upload Item</a>
                                        </li>
                                        <li>
                                            <a href="dashboard-manage-item.html">
                                                <span class="lnr lnr-book"></span>Manage Item</a>
                                        </li>
                                        <li>
                                            <a href="dashboard-withdrawal.html">
                                                <span class="lnr lnr-briefcase"></span>Withdrawals</a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="lnr lnr-exit"></span>Logout</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="text-center">
                                    <a href="signup.html" class="author-area__seller-btn inline">Become a Seller</a>
                                </div>
                            </div>
                        </div>
                        <!-- end /.mobile_content -->
                    </div>
                    <!-- end /.col-md-5 -->
                </div>
                <!-- end /.row -->
            </div>
            <!-- end /.container -->
        </div>
        <!-- end  -->

        <!-- start .mainmenu_area -->
        <div class="mainmenu">
            <!-- start .container -->
            <div class="container">
                <!-- start .row-->
                <div class="row">
                    <!-- start .col-md-12 -->
                    <div class="col-md-12">
                        <div class="navbar-header">
                            <!-- start mainmenu__search -->
                            <div class="mainmenu__search">
                                <form action="#">
                                    <div class="searc-wrap">
                                        <input type="text" placeholder="Search product">
                                        <button type="submit" class="search-wrap__btn">
                                            <span class="lnr lnr-magnifier"></span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <!-- start mainmenu__search -->
                        </div>

                        <nav class="navbar navbar-expand-md navbar-light mainmenu__menu">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                    <li class="has_dropdown">
                                        <a href="index.html">HOME</a>
                                    </li>
                                    <li class="has_dropdown">
                                        <a href="all-products-list.html">Forum</a>
                                        <div class="dropdowns dropdown--menu">
                                            <ul>
                                                <li>
                                                    <a href="all-products.html">All Discussions</a>
                                                </li>
                                                <li>
                                                    <a href="all-products.html">Popular Discussions</a>
                                                </li>
                                                <li>
                                                    <a href="index3.html">Saved Discussions</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="has_dropdown">
                                        <a href="#">Community Articles</a>
                                        <div class="dropdowns dropdown--menu">
                                            <ul>
                                                <li>
                                                    <a href="category-grid.html">Popular Items</a>
                                                </li>
                                                <li>
                                                    <a href="category-grid.html">Admin Templates</a>
                                                </li>
                                                <li>
                                                    <a href="category-grid.html">Blog / Magazine / News</a>
                                                </li>
                                                <li>
                                                    <a href="category-grid.html">Creative</a>
                                                </li>
                                                <li>
                                                    <a href="category-grid.html">Corporate Business</a>
                                                </li>
                                                <li>
                                                    <a href="category-grid.html">Resume Portfolio</a>
                                                </li>
                                                <li>
                                                    <a href="category-grid.html">eCommerce</a>
                                                </li>
                                                <li>
                                                    <a href="category-grid.html">Entertainment</a>
                                                </li>
                                                <li>
                                                    <a href="category-grid.html">Landing Pages</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="has_megamenu">
                                        <a href="#">Pages</a>
                                        <div class="dropdown_megamenu">
                                            <div class="megamnu_module">
                                                <div class="menu_items">
                                                    <div class="menu_column">
                                                        <ul>
                                                            <li class="title">Product</li>
                                                            <li>
                                                                <a href="all-products.html">Products Grid</a>
                                                            </li>
                                                            <li>
                                                                <a href="all-products-list.html">Products List</a>
                                                            </li>
                                                            <li>
                                                                <a href="category-grid.html">Category Grid</a>
                                                            </li>
                                                            <li>
                                                                <a href="category-list.html">Category List</a>
                                                            </li>
                                                            <li>
                                                                <a href="search-product.html">Search Product</a>
                                                            </li>
                                                            <li>
                                                                <a href="single-product.html">Single Product V1</a>
                                                            </li>
                                                            <li>
                                                                <a href="single-product-v2.html">Single Product V2</a>
                                                            </li>
                                                            <li>
                                                                <a href="single-product-v3.html">Single Product V3</a>
                                                            </li>
                                                            <li>
                                                                <a href="cart.html">Shopping Cart</a>
                                                            </li>
                                                            <li>
                                                                <a href="checkout.html">Checkout</a>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="menu_column">
                                                        <ul>
                                                            <li class="title">Author</li>
                                                            <li>
                                                                <a href="author.html">Author Profile</a>
                                                            </li>
                                                            <li>
                                                                <a href="author-items.html">Author Items</a>
                                                            </li>
                                                            <li>
                                                                <a href="author-reviews.html">Customer Reviews</a>
                                                            </li>
                                                            <li>
                                                                <a href="author-followers.html">Followers</a>
                                                            </li>
                                                            <li>
                                                                <a href="author-following.html">Following</a>
                                                            </li>
                                                            <li>
                                                                <a href="notification.html">Notifications</a>
                                                            </li>
                                                            <li>
                                                                <a href="message.html">Message Inbox</a>
                                                            </li>
                                                            <li>
                                                                <a href="message-compose.html">Message Compose</a>
                                                            </li>
                                                            <li>
                                                                <a href="favourites.html">Favorites Items</a>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="menu_column">
                                                        <ul>
                                                            <li class="title">Dashboard</li>
                                                            <li>
                                                                <a href="dashboard.html">Dashboard</a>
                                                            </li>
                                                            <li>
                                                                <a href="dashboard-setting.html">Account Settings</a>
                                                            </li>
                                                            <li>
                                                                <a href="dashboard-purchase.html">Author Purchases</a>
                                                            </li>
                                                            <li>
                                                                <a href="dashboard-add-credit.html">Add Credits</a>
                                                            </li>
                                                            <li>
                                                                <a href="dashboard-statement.html">Statements</a>
                                                            </li>
                                                            <li>
                                                                <a href="invoice.html">Invoice</a>
                                                            </li>
                                                            <li>
                                                                <a href="dashboard-upload.html">Upload Item</a>
                                                            </li>
                                                            <li>
                                                                <a href="dashboard-manage-item.html">Edit Items</a>
                                                            </li>
                                                            <li>
                                                                <a href="dashboard-withdrawal.html">Withdrawals</a>
                                                            </li>
                                                            <li>
                                                                <a href="dashboard-manage-item.html">Manage Items</a>
                                                            </li>
                                                            <li>
                                                                <a href="add-payment-method.html">Add Payment Method</a>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="menu_column">
                                                        <ul>
                                                            <li class="title">Customers</li>
                                                            <li>
                                                                <a href="support-forum.html">Support Forum</a>
                                                            </li>
                                                            <li>
                                                                <a href="support-forum-detail.html">Forum Details</a>
                                                            </li>
                                                            <li>
                                                                <a href="login.html">Login</a>
                                                            </li>
                                                            <li>
                                                                <a href="signup.html">Register</a>
                                                            </li>
                                                            <li>
                                                                <a href="recover-pass.html">Recovery Password</a>
                                                            </li>
                                                            <li>
                                                                <a href="customer-dashboard.html">Customer Dashboard</a>
                                                            </li>
                                                            <li>
                                                                <a href="customer-downloads.html">Customer Downloads</a>
                                                            </li>
                                                            <li>
                                                                <a href="customer-info.html">Customer Info</a>
                                                            </li>
                                                        </ul>

                                                        <ul>
                                                            <li class="title">Blog</li>
                                                            <li>
                                                                <a href="blog1.html">Blog V-1</a>
                                                            </li>
                                                            <li>
                                                                <a href="blog2.html">Blog V-2</a>
                                                            </li>
                                                            <li>
                                                                <a href="single-blog.html">Single Blog</a>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="menu_column">
                                                        <ul>
                                                            <li class="title">Other</li>
                                                            <li>
                                                                <a href="how-it-works.html">How It Works</a>
                                                            </li>
                                                            <li>
                                                                <a href="about.html">About Us</a>
                                                            </li>
                                                            <li>
                                                                <a href="pricing.html">Pricing Plan</a>
                                                            </li>
                                                            <li>
                                                                <a href="testimonial.html">Testimonials</a>
                                                            </li>
                                                            <li>
                                                                <a href="faq.html">FAQs</a>
                                                            </li>
                                                            <li>
                                                                <a href="affiliate.html">Affiliates</a>
                                                            </li>
                                                            <li>
                                                                <a href="term-condition.html">Terms &amp; Conditions</a>
                                                            </li>
                                                            <li>
                                                                <a href="event.html">Event</a>
                                                            </li>
                                                            <li>
                                                                <a href="event-detail.html">Event Detail</a>
                                                            </li>
                                                            <li class="has_badge">
                                                                <a href="badges.html">Badges</a>
                                                                <span class="badge">New</span>
                                                            </li>
                                                            <li>
                                                                <a href="404.html">404 Error page</a>
                                                            </li>
                                                            <li>
                                                                <a href="carieer.html">Job Posts</a>
                                                            </li>
                                                            <li>
                                                                <a href="job-detail.html">Job Details</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="contact.html">contact</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->
                        </nav>
                    </div>
                    <!-- end /.col-md-12 -->
                </div>
                <!-- end /.row-->
            </div>
            <!-- start .container -->
        </div>
        <!-- end /.mainmenu-->
    </div>
    <!-- end /.menu-area -->
    <!--================================
        END MENU AREA
    =================================-->



         
        <main class="py-4">
            @yield('content')
        </main>
    </div>
<footer>
    <div class="mini-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright-text">
                            <p>&copy; 2019
                                <a href="#">ACoSeP</a>. All rights reserved. Created by
                                <a href="#">Web Industry Solutions</a>
                            </p>
                        </div>

                        <div class="go_top">
                            <span class="lnr lnr-chevron-up"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


<script src="{{ asset('js/vendor/jquery/jquery-1.12.3.js') }}"></script>
    <script src="{{ asset('../js/vendor/jquery/popper.min.js') }}"></script>
    <script src="{{ asset('../js/vendor/jquery/uikit.min.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/jquery-ui.min.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/jquery.barrating.min.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/jquery.countdown.min.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/jquery.counterup.min.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/jquery.easing1.3.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/chart.bundle.min.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/grid.min.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/owl.carousel.min.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/slick.min.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/tether.min.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/trumbowyg.min.js') }}" defer></script>
    <script src="{{ asset('../js/vendor/waypoints.min.js') }}" defer></script>
    <script src="{{ asset('../js/dashboard.js') }}" defer></script>
    <script src="{{ asset('../js/main.js') }}" defer></script>
</body>
</html>
