@extends('layouts.app')

@section('content')
<!--================================
        START BREADCRUMB AREA
    =================================-->
    <section class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb">
                        <ul>
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li class="active">
                                <a href="#">Register</a>
                            </li>
                        </ul>
                    </div>
                    <h1 class="page-title">Register</h1>
                </div>
                <!-- end /.col-md-12 -->
            </div>
            <!-- end /.row -->
        </div>
        <!-- end /.container -->
    </section>
    <!--================================
        END BREADCRUMB AREA
    =================================-->
 <!--================================
            START SIGNUP AREA
    =================================-->
    <section class="signup_area section--padding2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="cardify signup_form">
                            <div class="login--header">
                                <h3>Create Your Account</h3>
                                <p>Please fill the following fields with appropriate information to register a new ACoSeP
                                    account.
                                </p>
                            </div>
                            <!-- end .login_header -->

                            <div class="login--form">
                            <div class="row">
                            <div class="col-lg-6">
 <div class="form-group">
                                    <label for="name">{{ __('Name') }}</label>
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Enter your Name" required autofocus>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                </div>
                                <!-- email -->
                                <div class="form-group">
                                    <label for="email">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Enter your email address" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                </div>

                                <!-- Job Title -->
                                <div class="form-group">
                                    <label for="jobtitle">{{ __('Job Title') }}</label>
                                    <input id="jobtitle" type="text" class="form-control{{ $errors->has('jobtitle') ? ' is-invalid' : '' }}" name="jobtitle" value="{{ old('jobtitle') }}" placeholder="Enter your current job title" required>

                                @if ($errors->has('jobtitle'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('jobtitle') }}</strong>
                                    </span>
                                @endif
                                </div>

                                <!-- Company -->
                                <div class="form-group">
                                    <label for="company">{{ __('Company') }}</label>
                                    <input id="company" type="text" class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}" name="company" value="{{ old('company') }}" placeholder="Enter the name of your current employer" required>

                                @if ($errors->has('company'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('company') }}</strong>
                                    </span>
                                @endif
                                </div>

                                <!-- License -->
                                <div class="form-group">
                                    <label for="license">{{ __('Security License *Optional') }}</label>
                                    <input id="license" type="text" class="form-control{{ $errors->has('license') ? ' is-invalid' : '' }}" name="license" value="{{ old('license') }}" placeholder="Enter your Security license">
                                </div>
                            </div>
                            <div class="col-lg-6">
     <!-- Phone -->
                                <div class="form-group">
                                    <label for="phone">{{ __('Phone number') }}</label>
                                    <input id="phone" type="number" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
 name="phone" value="{{ old('phone') }}" placeholder="Enter your phone number" required>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                                </div>

                                <!-- Country -->
                                 <div class="form-group">
                                <label for="country">Choose a Country:</label>
                                <select name="country" id="country" class="form-control">
                                <option value="">Choose one country</option>
                                    @foreach (App\Country::all() as $country)
                                        <option value="{{ $country->countrycode }}" {{ old('countrycode') == $country->countrycode ? 'selected' : '' }}>
                                            {{ $country->countryname }}
                                        </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('country'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                                </div>

                                <!-- Password -->
                                <div class="form-group">
                                    <label for="password">{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Enter your password..." required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                </div>


                                <div class="form-group">
                                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm password" required>
                                </div>
                            </div>
              </div>
                               


                           

                                <button class="btn btn--md btn--round register_btn" type="submit">{{ __('Register') }}</button>

                                <div class="login_assist">
                                    <p>Already have an account?
                                        <a href="{{ route('login') }}">{{ __('Login') }}</a>
                                    </p>
                                </div>
                            </div>
                            <!-- end .login--form -->
                        </div>
                        <!-- end .cardify -->
                    </form>
                </div>
                <!-- end .col-md-6 -->
            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </section>
    <!--================================
            END SIGNUP AREA
    =================================-->




@endsection



