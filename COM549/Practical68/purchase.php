<?php
include("book_sc_fns.php");
session_start();
do_html_header("Checkout");

// Info
$name = $_POST['name'];
$address = $_POST['address'];
$city = $_POST['city'];
$zip = $_POST['zip'];
$country = $_POST['country'];

// Check if filled out 
if (($_SESSION['cart']) && ($name) && ($address) && ($city) && ($zip) && ($country)) {
    if ((insert_order($_POST)) != false) {
        display_cart($_SESSION['cart'], false, 0); 
        display_shipping(calculate_shipping_cost());
         
        display_card_form($name);
        display_button('show_cart.php', 'continue-shopping', 'Continu Shopping'); 
    }else {
      echo "<p>Could not store data, please try again.</p><hr />";
      display_button('checkout.php', 'back', 'back');  
    }
}else {
    echo "<p>You did not fill in all the fields, please try again.</p><hr />";
    display_button('checkout.php', 'back', 'back');
}

do_html_footer();
?>