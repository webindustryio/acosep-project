<?php
require_once("book_sc_fns.php");

// The shopping cart needs sessions, so start one 
session_start();
// Display header 
do_html_header("Book-store");
// Show cat title
echo "<p>Please chose a category: </p>";

$cat_array = get_categories();
display_categories($cat_array);

// If login as admin, show add, delete, edit cat link
if (isset($_SESSION['admin_user'])) {
    display_button("admin.php", "admin-menu", "Admin Menu");
}
do_html_footer(); 
?>