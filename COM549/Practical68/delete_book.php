<?php
require_once("book_sc_fns.php");
session_start();
do_html_header("Deleting book");

if (check_admin_user()) {
    if (isset($_POST['isbn'])) {
        if (delete_book($_POST['isbn'])) {
            echo "<p>Book was deleted.</p>";
        }else {
            echo "<p>Book could not be deleted.</p>";
        }
    }else {
        echo "<p>No Book specified. Please try again</p>";
    }
    do_html_url("admin.php", "Back to administation menu");
}else {
    echo "<p>You are not authorized to enter the administration area.</p>";
}



do_html_footer();
?>