<!DOCTYPE html>
<html>
<head>
<title>The Guardian</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/animate.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<style>
.display-4{
    margin-top:40px !important;
}
.newsBox{
  width: 100% !important;
  height: 500px !important;
  background: url('img/loading.svg');
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center; 
}
.btn-primary{
  background-color: #0d4d7c;
  border-color: #98b8d3;
}
.btn-primary:hover{
  color: #0d4d7c;
  background-color: #98b8d3;
}
.card-title{
  color: #0d4d7c;
}
.card{
min-height: 550px !important;
}
.dropdown-item.active, .dropdown-item:active{
  background-color:#0d4d7c;
}
</style>
</head>

?>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Web Applications Development</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Projects
        </a>
        <div class="dropdown-menu"  aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical1/practical1.php">1. Currency Converter</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical2/practical2.html">2. Registration form</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical3/practical3.php">3. Retrieving Weather Information</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical4/practical4.php">4. Youtube Data API Integration</a>
          <a class="dropdown-item active" href="http://webindustry.io/COM549/Practical5/practical5.php">5. Guardian news API</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical6/">6. Shopping Cart</a>
        </div>
      </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="jumbotron jumbotron-fluid text-center">
  <div class="container">
  <div style="align:inline"><img class="display-4 img-fluid animated fadeInLeft" width="350px" src="img/theguardian.png"><img class="display-4 img-fluid animated fadeInRight" width="250px" src="img/Ulster-University.png"></div>
    <p class="lead animated fadeIn">PHP The Guardian API - Practical 5</p>
</div>
  </div>
<div class="container">
<form method="post">
<div class="row">
  <div class="col-md-8">
  <div class="form-group">
    <label for="exampleInputEmail1">Search for a term</label>
    <input class="form-control" required type="search" id="searchBox" name="searchBox" placeholder="Search...">
  </div>
</div>
  <div class="col-md-2">
  <div class="form-group">
    <label for="exampleInputEmail1">Max results</label>
    <input type="number" class="form-control" id="maxResults" name="maxResults" min="1" max="50" step="1" value="9">
  </div>
</div>
  <div class="col-md-2">
  <div class="form-group">
  <label for="exampleInputEmail1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
<button class="btn btn-primary btn-block" type="submit" name="submit">Search</button>
</div>
</div>
</div>
</br>
</form>

<?php

require 'Carbon/autoload.php';

//loads dependency to Carbon library
use Carbon\Carbon;
use Carbon\CarbonInterval;


if (isset($_POST['submit'])) {
$apiKey = '0f364c2e-bd3f-4d7a-8b8c-7186206e3283'; //developer API Key
$seachKey = urlencode( $_POST["searchBox"]); //urlencode for protection against SQL Injection attacks
$itemsNumber = $_POST["maxResults"]; //number of results chosen by the user to display

$request = "http://content.guardianapis.com/search?format=json&api-key=" .$apiKey . "&q=" . $seachKey . "&page-size=" . $itemsNumber ."&page=1&show-fields=starRating,headline,thumbnail,trailText,short-url";

echo ('<nav aria-label="breadcrumb">
<ol class="breadcrumb animated fadeInUp">
  <li class="breadcrumb-item active" aria-current="page">'.$itemsNumber.' results for the term <strong> '. $seachKey .' </strong></li>
</ol>
</nav>');

// "https://content.guardianapis.com/search?q=12%20years%20a%20slave&format=json&tag=film/film,tone/reviews&from-date=2010-01-01&show-tags=contributor&show-fields=starRating,headline,thumbnail,trailText,short-url&order-by=relevance&api-key=test"; 
$response  = file_get_contents($request);
$jsonobj  = json_decode($response);

//fetch Json format response from the API endpoint
echo ('<div class="row">');           
 for($i=0; $i<$itemsNumber; $i++)
 //loop through the Json format array
 {
   $id = $jsonobj ->response->results[$i]->id;
   $webTitle = $jsonobj ->response->results[$i] ->webUrl;
   $headline=$jsonobj ->response->results[$i]->fields->headline;
   $trailText = $jsonobj ->response->results[$i]->fields->trailText;
   $shortUrl = $jsonobj ->response->results[$i]->fields->shortUrl;
   $s1=$jsonobj ->response->results[$i]->fields->thumbnail;
   $publicationDate = $jsonobj ->response->results[$i]->webPublicationDate;
   $publication = Carbon::now()->diffForHumans($publicationDate, true, null).' ago'; //Carbon diffForHumans time and date format
  
   $modalId = $id[6];
   //display results in Bootstrap cards
   echo('<div class="col-md-4"><div class="card animated fadeInUp" style="margin-bottom: 20px; width: 100%">');
   echo('<img src="'.$s1.'" class="card-img-top" alt="...">');
   echo('<div class="card-body">');
   echo('<h5 class="card-title">'.$headline.'</h5>');
   echo('<p class="card-text">'.$trailText.'</p>');
   echo('<a href="'.$shortUrl.'" target="_blank" class="btn btn-primary">Read article</a>');
   echo('</div><div class="card-footer text-muted">'.$publication.'</div></div></div>');
   
 }
 echo ('</div>');
}
?>
</div>
</br>
</div>
</body>
</html>




