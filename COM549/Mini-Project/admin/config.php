<?php
// HTTP
define('HTTP_SERVER', 'http://webindustry.io/COM549/Mini-Project/admin/');
define('HTTP_CATALOG', 'http://webindustry.io/COM549/Mini-Project/');

// HTTPS
define('HTTPS_SERVER', 'http://webindustry.io/COM549/Mini-Project/admin/');
define('HTTPS_CATALOG', 'http://webindustry.io/COM549/Mini-Project/');

// DIR
define('DIR_APPLICATION', '/opt/bitnami/apache2/htdocs/COM549/Mini-Project/admin/');
define('DIR_SYSTEM', '/opt/bitnami/apache2/htdocs/COM549/Mini-Project/system/');
define('DIR_IMAGE', '/opt/bitnami/apache2/htdocs/COM549/Mini-Project/image/');
define('DIR_CATALOG', '/opt/bitnami/apache2/htdocs/COM549/Mini-Project/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_SYSTEM . 'storage/cache/');
define('DIR_DOWNLOAD', DIR_SYSTEM . 'storage/download/');
define('DIR_LOGS', DIR_SYSTEM . 'storage/logs/');
define('DIR_MODIFICATION', DIR_SYSTEM . 'storage/modification/');
define('DIR_UPLOAD', DIR_SYSTEM . 'storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'Ulster2019&');
define('DB_DATABASE', 'miniproject');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
