<!DOCTYPE html>
<html>
<head>
<title>Currency Convertor</title>
<link rel="stylesheet" href="http://webindustry.io/COM549/Practical1/css/bootstrap.min.css">
<link rel="stylesheet" href="http://webindustry.io/COM549/Practical1/css/animate.css">
<script src="http://webindustry.io/COM549/Practical1/js/bootstrap.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<style>
.display-4{
    margin-top:40px !important;
}
</style>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Web Applications Development</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Projects
        </a>
        <div class="dropdown-menu"  aria-labelledby="navbarDropdown">
          <a class="dropdown-item active" href="http://webindustry.io/COM549/Practical1/practical1.php">1. Currency Converter</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical2/practical2.html">2. Registration form</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical3/practical3.php">3. Retrieving Weather Information</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical4/practical4.php">4. Youtube Data API Integration</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical5/practical5.php">5. Guardian news API</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical6/">6. Shopping Cart</a>
        </div>
      </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="jumbotron jumbotron-fluid text-center">
  <div class="container">
    <img class="display-4 img-fluid" width="300px" src="http://webindustry.io/COM549/Practical1/img/Ulster-University.png">
    <h1 class="display-4">Currency Convertor</h1>
    <p class="lead">PHP Currency Convertor web application baseed on EX.com - The World's Trusted Currency Authority</p>
</div>
  </div>
  <?php
if(isset($_POST['submit'])){
 
$amount = $_POST['amount'];
$fromCurr = $_POST['fromCurr'];
$toCurr = $_POST['toCurr'];

/*** USD */
$USDtoEUR = 0.88398; $USDtoGBP = 0.76603; $USDtoINR = 71.7140; $USDtoAUD = 1.38605; $USDtoCAD = 1.31317; $USDtoSGD = 1.35315;
/*** EUR */
$EURtoUSD = 1.14358; $EURtoGBP = 0.876031; $EURtoINR = 81.9957; $EURtoAUD = 1.58468; $EURtoCAD = 1.50161; $EURtoSGD = 1.54726;
/*** GBP */
$GBPtoUSD = 1.30523; $GBPtoEUR = 1.14157; $GBPtoINR = 93.5661; $GBPtoAUD = 1.80847; $GBPtoCAD = 1.71398; $GBPtoSGD = 1.76592;
/*** INR */
$INRtoUSD = 0.0139543; $INRtoEUR = 0.0122064; $INRtoGBP = 0.0122064; $INRtoAUD = 0.0193367; $INRtoCAD = 0.0193367; $INRtoSGD = 0.0188828;
/*** AUD */
$AUDtoUSD = 0.0188828; $AUDtoEUR = 0.631326; $AUDtoGBP = 0.553250; $AUDtoINR = 51.7108; $AUDtoCAD = 0.947665; $AUDtoSGD = 0.976378;
/*** CAD */
$CADtoUSD = 0.761367; $CADtoEUR = 0.666121; $CADtoGBP = 0.583846; $CADtoINR = 54.5764; $CADtoAUD = 1.05533; $CADtoSGD = 1.03046;
/*** SGD */
$SGDtoUSD = 0.761459; $SGDtoEUR = 0.646327; $SGDtoGBP = 0.566604; $SGDtoINR = 52.9583; $SGDtoAUD = 1.02400; $SGDtoCAD = 0.970581;

/** USD */

if($fromCurr=="USD" AND $toCurr=="USD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "$$amount is worth " . "$" . $amount* 1 . " at a rate of 1.00" . "</center>";
  echo '</div></div>';
    }

if($fromCurr=="USD" AND $toCurr=="EUR"){
echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
echo "<center><b>Your Converted Amount is:</b><br></center>";
echo "<center>" . "$$amount is worth " . "€" . $amount* $USDtoEUR . " at a rate of $USDtoEUR" . "</center>";
echo '</div></div>';
}

if($fromCurr=="USD" AND $toCurr=="GBP"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "$$amount is worth " . "£" . $amount* $USDtoGBP . " at a rate of $USDtoGBP" . "</center>";
  echo '</div></div>';
  }

if($fromCurr=="USD" AND $toCurr=="INR"){
    echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
    echo "<center><b>Your Converted Amount is:</b><br></center>";
    echo "<center>" . "$$amount is worth " . "₹" . $amount* $USDtoINR . " at a rate of $USDtoINR" . "</center>";
    echo '</div></div>';
    }

if($fromCurr=="USD" AND $toCurr=="AUD"){
      echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
      echo "<center><b>Your Converted Amount is:</b><br></center>";
      echo "<center>" . "$$amount is worth " . "A$" . $amount* $USDtoAUD . " at a rate of $USDtoAUD" . "</center>";
      echo '</div></div>';
      }

if($fromCurr=="USD" AND $toCurr=="CAD"){
        echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
        echo "<center><b>Your Converted Amount is:</b><br></center>";
        echo "<center>" . "$$amount is worth " . "C$" . $amount* $USDtoCAD . " at a rate of $USDtoCAD" . "</center>";
        echo '</div></div>';
        }

if($fromCurr=="USD" AND $toCurr=="SGD"){
          echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
          echo "<center><b>Your Converted Amount is:</b><br></center>";
          echo "<center>" . "$$amount is worth " . "S$" . $amount* $USDtoSGD . " at a rate of $USDtoSGD" . "</center>";
          echo '</div></div>';
      }

/*** EUR */

if($fromCurr=="EUR" AND $toCurr=="EUR"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "€$amount is worth " . "€" . $amount* 1 . " at a rate of 1.00" . "</center>";
  echo '</div></div>';
    }

if($fromCurr=="EUR" AND $toCurr=="USD"){
      echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
      echo "<center><b>Your Converted Amount is:</b><br></center>";
      echo "<center>" . "€$amount is worth " . "$" . $amount* $EURtoUSD . " at a rate of $EURtoUSD" . "</center>";
      echo '</div></div>';
}

if($fromCurr=="EUR" AND $toCurr=="GBP"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "€$amount is worth " . "£" . $amount* $EURtoGBP . " at a rate of $EURtoGBP" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="EUR" AND $toCurr=="INR"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "€$amount is worth " . "₹" . $amount* $EURtoINR . " at a rate of $EURtoINR" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="EUR" AND $toCurr=="AUD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "€$amount is worth " . "A$" . $amount* $EURtoAUD . " at a rate of $EURtoAUD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="EUR" AND $toCurr=="CAD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "€$amount is worth " . "C$" . $amount* $EURtoCAD . " at a rate of $EURtoCAD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="EUR" AND $toCurr=="SGD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "€$amount is worth " . "S$" . $amount* $EURtoSGD . " at a rate of $EURtoSGD" . "</center>";
  echo '</div></div>';
}

/*** GBP */

if($fromCurr=="GBP" AND $toCurr=="GBP"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "£$amount is worth " . "£" . $amount* 1 . " at a rate of 1.00" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="GBP" AND $toCurr=="USD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "£$amount is worth " . "$" . $amount* $GBPtoUSD . " at a rate of $GBPtoUSD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="GBP" AND $toCurr=="EUR"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "£$amount is worth " . "€" . $amount* $GBPtoEUR . " at a rate of $GBPtoEUR" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="GBP" AND $toCurr=="INR"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "£$amount is worth " . "₹" . $amount* $GBPtoINR . " at a rate of $GBPtoINR" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="GBP" AND $toCurr=="AUD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "£$amount is worth " . "A$" . $amount* $GBPtoAUD . " at a rate of $GBPtoAUD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="GBP" AND $toCurr=="CAD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "£$amount is worth " . "C$" . $amount* $GBPtoCAD . " at a rate of $GBPtoCAD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="GBP" AND $toCurr=="SGD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "£$amount is worth " . "S$" . $amount* $GBPtoSGD . " at a rate of $GBPtoSGD" . "</center>";
  echo '</div></div>';
}

/*** INR */

if($fromCurr=="INR" AND $toCurr=="USD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "₹$amount is worth " . "₹" . $amount* 1 . " at a rate of 1.00" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="INR" AND $toCurr=="EUR"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "₹$amount is worth " . "€" . $amount* $INRtoEUR . " at a rate of $INRtoEUR" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="INR" AND $toCurr=="GBP"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "₹$amount is worth " . "£" . $amount* $INRtoGBP . " at a rate of $INRtoGBP" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="INR" AND $toCurr=="AUD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "₹$amount is worth " . "A$" . $amount* $INRtoAUD . " at a rate of $INRtoAUD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="INR" AND $toCurr=="CAD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "₹$amount is worth " . "C$" . $amount* $INRtoCAD . " at a rate of $INRtoCAD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="INR" AND $toCurr=="SGD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "₹$amount is worth " . "S$" . $amount* $INRtoSGD . " at a rate of $INRtoSGD" . "</center>";
  echo '</div></div>';
}

/*** AUD */

if($fromCurr=="AUD" AND $toCurr=="AUD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "A$$amount is worth " . "A$" . $amount* 1 . " at a rate of 1.00" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="AUD" AND $toCurr=="USD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "A$$amount is worth " . "$" . $amount* $AUDtoUSD . " at a rate of $AUDtoUSD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="AUD" AND $toCurr=="EUR"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "A$$amount is worth " . "€" . $amount* $AUDtoEUR . " at a rate of $AUDtoEUR" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="AUD" AND $toCurr=="GBP"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "A$$amount is worth " . "£" . $amount* $AUDtoGBP . " at a rate of $AUDtoGBP" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="AUD" AND $toCurr=="INR"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "A$$amount is worth " . "₹" . $amount* $AUDtoINR . " at a rate of $AUDtoINR" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="AUD" AND $toCurr=="CAD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "A$$amount is worth " . "C$" . $amount* $AUDtoCAD . " at a rate of $AUDtoCAD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="AUD" AND $toCurr=="SGD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "A$$amount is worth " . "S$" . $amount* $AUDtoSGD . " at a rate of $AUDtoSGD" . "</center>";
  echo '</div></div>';
}

/*** CAD */

if($fromCurr=="CAD" AND $toCurr=="CAD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "C$$amount is worth " . "C$" . $amount* 1 . " at a rate of 1.00" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="CAD" AND $toCurr=="USD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "C$$amount is worth " . "$" . $amount* $CADtoUSD . " at a rate of $CADtoUSD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="CAD" AND $toCurr=="EUR"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "C$$amount is worth " . "€" . $amount* $CADtoEUR . " at a rate of $CADtoEUR" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="CAD" AND $toCurr=="GBP"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "C$$amount is worth " . "£" . $amount* $CADtoGBP . " at a rate of $CADtoGBP" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="CAD" AND $toCurr=="INR"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "C$$amount is worth " . "₹" . $amount* $CADtoINR . " at a rate of $CADtoINR" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="CAD" AND $toCurr=="AUD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "C$$amount is worth " . "A$" . $amount* $CADtoAUD . " at a rate of $CADtoAUD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="CAD" AND $toCurr=="SGD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "C$$amount is worth " . "S$" . $amount* $CADtoSGD . " at a rate of $CADtoSGD" . "</center>";
  echo '</div></div>';
}

/*** SGD */

if($fromCurr=="SGD" AND $toCurr=="SGD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "S$$amount is worth " . "S$" . $amount* 1 . " at a rate of 1.00" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="SGD" AND $toCurr=="USD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "S$$amount is worth " . "$" . $amount* $SGDtoUSD . " at a rate of $SGDtoUSD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="SGD" AND $toCurr=="EUR"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "S$$amount is worth " . "€" . $amount* $SGDtoEUR . " at a rate of $SGDtoEUR" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="SGD" AND $toCurr=="GBP"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "C$$amount is worth " . "£" . $amount* $CADtoGBP . " at a rate of $CADtoGBP" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="SGD" AND $toCurr=="INR"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "S$$amount is worth " . "₹" . $amount* $SGDtoINR . " at a rate of $SGDtoINR" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="SGD" AND $toCurr=="AUD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "S$$amount is worth " . "A$" . $amount* $SGDtoAUD . " at a rate of $SGDtoAUD" . "</center>";
  echo '</div></div>';
}

if($fromCurr=="SGD" AND $toCurr=="CAD"){
  echo '<div class="container"><div class="alert alert-primary animated fadeIn" role="alert">';
  echo "<center><b>Your Converted Amount is:</b><br></center>";
  echo "<center>" . "C$$amount is worth " . "S$" . $amount* $SGDtoCAD . " at a rate of $SGDtoCAD" . "</center>";
  echo '</div></div>';
}


}
 
?>
<div class="container">
<form action="" method="post">
  <div class="form-group">
    <label for="insertCurrency">Insert amount</label>
    <input type="number" name="amount" class="form-control" required id="insertCurrency" aria-describedby="currency" placeholder="Enter amount">
    <small id="insertCurrency" class="form-text text-muted">Insert only the currency desire</small>
</div>
<div class="row">
  <div class="col col-6">
    <div class="form-group">
      <label for="insertCurrency">Insert amount</label>
      <select class="form-control" name="fromCurr">
      <option value="USD">USD - United States Dollar</option>
        <option value="EUR">EUR - Euro </option>
        <option value="GBP">GBP - British Pound</option>
        <option value="INR">INR - Indian Rupee</option>
        <option value="AUD">AUD - Australian Dollar</option>
        <option value="CAD">CAD - Canadian Dollar</option>
        <option value="SGD"> SGD - Singapore Dollar</option>
        </select>
  </div>
  </div>
  <div class="col col-6">
    <div class="form-group">
      <label for="convertedCurrency">Converted amount</label>
      <select class="form-control" name="toCurr">
        <option value="USD">USD - United States Dollar</option>
        <option value="EUR">EUR - Euro </option>
        <option value="GBP">GBP - British Pound</option>
        <option value="INR">INR - Indian Rupee</option>
        <option value="AUD">AUD - Australian Dollar</option>
        <option value="CAD">CAD - Canadian Dollar</option>
        <option value="SGD"> SGD - Singapore Dollar</option>
        </select>
  </div>
  </div>
  </div>
  <button class="btn btn-block btn-secondary" type='submit' name='submit' value="CovertNow">Calculate</button></br>
</form>  
</div>
</body>
</html>