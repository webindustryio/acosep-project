<!DOCTYPE html>
<html>
<head>
<title>Weather Information</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/animate.css">
<script src="js/bootstrap.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<style>
.display-4{
    margin-top:40px !important;
}
.progress{
  background-color: #ffffff;
}
.jumbotron{
  background: url('img/background-hero.jpg');
  background-size: cover;
  text-shadow: 1px 0px 4px rgba(150, 150, 150, 1);
  color: #ffffff;
}
</style>
</head>
<?php
//uses the PHP functionality fopen() to open the document
$myFile = "http://scm.ulster.ac.uk/weather/Realtime.txt";
//$myFile = "http://webindustry.io/COM549/Practical3/Realtime.txt";
//reads opened document using the fread() PHP functionality
$fh = fopen($myFile, 'r');
//each line is represented by passing in the file and the amount of characters we want to read from the file
$weatherDate = fread($fh, 8);
$weatherTime = fread($fh, 9);
$weatherOutsideTemparature = fread($fh, 4);
$weatherRelativeHumidity = fread($fh, 4);
$weatherDewPoint = fread($fh, 4);
$weatherWindSpeed = fread($fh, 4);
$weatherLwindSpeed = fread($fh, 4);
$weatherWindBearing = fread($fh, 2);
$weatherCurrentRainRate = fread($fh, 4);
$weatherRainRate = fread($fh, 3);
$weatherBarometer = fread($fh, 7);
$weatherCurrentWindDirection = fread($fh, 3);
$windSpeed = fread($fh, 3);
$emptyValues = fread($fh, 12);
$windRun = fread($fh, 4);
$preasureTrendValue = fread($fh, 5);
$monthlyRainFall = fread($fh, 5);
$yearlyRainFall = fread($fh, 5);
$yesterdayRainFall = fread($fh, 5);
$insideTemperature = fread($fh, 5);
$insideHumidity = fread($fh, 3);
$windchill = fread($fh, 4);
$temperatureTrendValue = fread($fh, 5);
$todayHighTemperature = fread($fh, 4);
echo $progressBarTemp;
fclose($fh);
?>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Web Applications Development</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Projects
        </a>
        <div class="dropdown-menu"  aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical1/practical1.php">1. Currency Converter</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical2/practical2.html">2. Registration form</a>
          <a class="dropdown-item active" href="http://webindustry.io/COM549/Practical3/practical3.php">3. Retrieving Weather Information</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical4/practical4.php">4. Youtube Data API Integration</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical5/practical5.php">5. Guardian news API</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical6/">6. Shopping Cart</a>
        </div>
      </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="jumbotron jumbotron-fluid">
  <div class="container">
    <br></br>
    <br></br>
    <center><h1>WEATHER INFORMATION</h1></center>
    <center><h3>PHP Practical 3</h3></center>
    <br></br>
  <div class="row">
    <div class="col col-md-4">
    <h4>Outside Temperature: <?php echo $weatherOutsideTemparature ?> C<sup>o</sup></h4>
</div>
    <div class="col col-md-8">
    <div class="progress" style="height: 90%">
  <div class="progress-bar" role="progressbar progress-bar-striped progress-bar-animated" style="width:<?php echo $weatherOutsideTemparature?>%" aria-valuenow="<?php echo $weatherOutsideTemparature ?>" aria-valuemin="0" aria-valuemax="100"><?php echo $weatherOutsideTemparature ?></div>
</div>
</div>
</div>
<br></br>
    <br></br>
    <br></br>
    <br></br>
    <br></br>
    <br></br>
    <br></br>
    <br></br>
  </div>
</div>

<div class="container">
<div class="row">
<div class="col-md-6">
<div class="card">
  <div class="card-header">
   <strong>Date and Time</strong>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Date: <?php echo $weatherDate ?></li>
    <li class="list-group-item">Time: <?php echo $weatherTime ?></li>
  </ul>
</div>
</br>
<div class="card">
  <div class="card-header">
    <strong>Humidity and Temperature</strong>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Relative Humidity: <?php echo $weatherRelativeHumidity ?> %</li>
    <li class="list-group-item">Dewpoint: <?php echo $weatherDewPoint ?> C</li>
    <li class="list-group-item">Barometer (sea level pressure): <?php echo $weatherBarometer ?> mb</li>
    <li class="list-group-item">Preasure trend value: <?php echo $preasureTrendValue ?> mb</li>
    <li class="list-group-item">Inside temperature: <?php echo $insideTemperature ?></li>
    <li class="list-group-item">Inside humidity: <?php echo $insideHumidity?> %</li>
    <li class="list-group-item">Temperature trend value: <?php echo $temperatureTrendValue?> C</li>
    <li class="list-group-item">Today's high temperature: <?php echo $todayHighTemperature?> C</li>
  </ul>
</div>
</div>
<div class="col-md-6">
<div class="card">
  <div class="card-header">
    <strong>Wind</strong>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Wind speed (average): <?php echo $weatherWindSpeed ?> mph</li>
    <li class="list-group-item">Latest wind speed reading: <?php echo $weatherLwindSpeed ?> mph</li>
    <li class="list-group-item">Wind bearing (degrees): <?php echo $weatherWindBearing ?> <sup>o</sup></li>
    <li class="list-group-item">Wind speed (beaufort): <?php echo $windSpeed ?></li>
    <li class="list-group-item">Wind run (today): <?php echo $windRun ?></li>
    <li class="list-group-item">Current wind direction: <?php echo $weatherCurrentWindDirection ?></li>
    <li class="list-group-item">Wind chill: <?php echo $insideHumidity?> C</li>
  </ul>
</div>
</br>
<div class="card">
  <div class="card-header">
   <strong>Rain</strong>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Current rain rate (per hour): <?php echo $weatherCurrentRainRate ?> mm</li>
    <li class="list-group-item">Rain today: <?php echo $weatherRainRate ?> mm</li>
    <li class="list-group-item">Monthly rainfall: <?php echo $monthlyRainFall ?> mm</li>
    <li class="list-group-item">Yearly rainfall: <?php echo $yearlyRainFall ?> mm</li>
    <li class="list-group-item">Yesterday's rainfall: <?php echo $yesterdayRainFall ?> mm</li>
  </ul>
</div>
</div>
</div>
</div>
<br></br>
</body>
</html>