<?php 

require_once('book_sc_fns.php');
session_start();

do_html_header("Add a book");
if(check_admin_user()) {
	//checks if the user is logged in and if it has admin credentials
	display_book_form(); //insert book form
	do_html_url("admin.php", "Back to administration menu");
} else {
	echo "<p>You are not authorized to enter the administration area.</p>";
}
do_html_footer();

?>