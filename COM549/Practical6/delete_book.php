<?php  

require_once('book_sc_fns.php');
session_start();

do_html_header("Deleting book");
if(check_admin_user()) {
	//check if the user is logged in and most importantly, if it has admin credentials
	if(isset($_POST['isbn'])) {
		$isbn = $_POST['isbn'];
		//uses the primary key of the book "isbn" to delete the object
		if(delete_book($isbn)) {
			//checks if the book has been deleted
			echo "<p>Book ".htmlspecialchars($isbn)." was deleted.</p>";
		} else {
			//error in deleting the book
			echo "<p>Book ".htmlspecialchars($isbn)." could not be deleted.</p>";
		}
	} else {
		//prompts the user that it needs an ISBN number to delete the object
		echo "<p>We need an ISBN to delete a book. Please try again.</p>";
	}
	do_html_url("admin.php", "Back to administration menu"); //admin url
} else {
	echo "<p>You are not authorised to view this page.</p>";
	//if user is not admin, it is informed that it is not allowed on this page
}

do_html_footer();

?>