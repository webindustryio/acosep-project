<?php 
//get all the categories from the database
function get_categories() {
	$conn = db_connect(); //connect to the database
	$query = "select catid, catname from categories"; //get all the categories query
	$result = @$conn->query($query); //establish the connection and pass on the query
	if(!$result) {
		return false;
	}
	$num_cats = @$result->num_rows;
	if($num_cats == 0) {
		return false;
	}
	$result = db_result_to_array($result);
	return $result;
}
//get the category name
function get_category_name($catid) {
	$conn = db_connect();
	$query = "select catname from categories
	where catid = '".$conn->real_escape_string($catid)."'";

	$result = @$conn->query($query);
	if(!$result) {
		return false;
	}
	$num_cats = @$result->num_rows;
	if($num_cats == 0) {
		return false;
	}
	$row = $result->fetch_object();
	return $row->catname;
}
//get all the books from a particular category id
function get_books($catid) {
	if((!$catid) || ($catid == '')) {
		return false;
	}

	$conn = db_connect();
	$query = "select * from books where catid='".$conn->real_escape_string($catid)."'";
	$result = @$conn->query($query);
	if(!$result) {
		return false;
	}
	$num_books = @$result->num_rows;
	if($num_books == 0) {
		return false;
	}
	$result = db_result_to_array($result);
	return $result;
}

//get the details of the books by passing in the isbn unique id of the book
function get_book_details($isbn) {
	if((!$isbn) || ($isbn == '')) {
		return false;
	}
	$conn = db_connect();
	$query = "select * from books where isbn='".$conn->real_escape_string($isbn)."'";
	$result = @$conn->query($query);
	if(!$result) {
		return false;
	}
	$result = @$result->fetch_assoc();
	return $result;
}
//gets all the elements added in the cart and calculates the price
function calculate_price($cart) {
	$price = 0.0;
	if(is_array($cart)) {
		$conn = db_connect();
		foreach($cart as $isbn => $qty) {
			$query = "select price from books where isbn='".$conn->real_escape_string($isbn)."'";
			$result = $conn->query($query);
			if($result) {
				$item = $result->fetch_object();
				$item_price = $item->price;
				$price += $item_price*$qty;
			}
		}
	}
	return $price;
}


function calculate_items($cart) {
	$items = 0;
	if(is_array($cart)) {
		foreach($cart as $isbn => $qty) {
			$items += $qty;
		}
	}
	return $items;
}

function calculate_shipping_cost() {
	return 20.00;
}
?>