<?php
include ('book_sc_fns.php');
session_start();

$isbn = $_GET['isbn'];

$book = get_book_details($isbn);

do_html_header($book['title']);
display_book_details($book);

$target = "index.php";
if($book['catid']) {
	//gets the books from the database based on the catergory id
	$target = "show_cat.php?catid=".urlencode($book['catid']); //uses url encode for security purposes
	//urlencode protects attackers from running sql injection queries
}

if(check_admin_user()) {
	//if logged in as admin, display admin functions such as edit, delete
	display_button("edit_book_form.php?isbn=".urlencode($isbn), "edit-item", "Edit Item");
	display_button("admin.php", "admin-menu", "Admin Menu");
	display_button($target, "continue", "Continue");
} else {
	//if user not logged in as admin, hide the admin buttons and display only the add to cart and continue buttons
	display_button("show_cart.php?new=".urlencode($isbn), "add-to-cart", "Add ".htmlspecialchars($book['title']." To My Shopping Cart"));
	display_button($target, "continue-shopping", "Continue Shopping");
}

do_html_footer();
?>