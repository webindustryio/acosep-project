<?php  

require_once('book_sc_fns.php');
session_start();
//takes the current existing admin session
$old_user = $_SESSION['admin_user'];
unset($_SESSION['admin_user']);
session_destroy(); //logs out the user by destroying the current session

do_html_header("Logging Out");

if(!empty($old_user)) {
	//if the user is no longer logged in, displays the success message
	echo "<p>Logged out.</p>";
	do_html_url("login.php", "Login");
} else {
	//checks if the user has been logged in previously
	echo "<p>You were not logged in, and so have not been logged out.</p>";
	do_html_url("login.php", "Login"); //url to login
}

do_html_footer();

?>