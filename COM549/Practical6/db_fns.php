<?php

function db_connect() {
	include(__DIR__.'/../../../db_rc/dbconnect.php');

	//Connects the php project to the MySQL database
	$result = new mysqli('localhost', 'book_sc', 'Ulster2020&', 'book_sc');
	if(!$result) {
		//checks if the connection has been established
		return false;
	}
	$result->autocommit(TRUE);
	return $result;
}

//fetches the database results in an array and passes the array through a for loop
function db_result_to_array($result) {
	$res_array = array();

	for($count=0; $row=$result->fetch_assoc(); $count++) {
		$res_array[$count] = $row;
	}

	return $res_array; //return array
}
?>