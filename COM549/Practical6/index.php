<?php
	include_once 'book_sc_fns.php';

	session_start();
	do_html_header("Welcome to Book-O-Rama");

	echo "<p>Please choose a category</p>";

	$cat_array = get_categories(); //get_categories is a function in the book_sc_fns.php that fetches all the categories in the database

	display_categories($cat_array); //displays categories fetched from the db

	if(isset($_SESSION['admin_user'])) {
		//if logged in as admin, the Admin Menu will be displayed
		display_button("admin.php", "admin-menu", "Admin Menu");
	}
	do_html_footer();
?>