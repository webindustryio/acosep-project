<?php

// include function files for this application
require_once('book_sc_fns.php');
session_start();

do_html_header("Updating book");
if (check_admin_user()) {
	//checks if the user is logged in and has admin credentials
	if (filled_out($_POST)) {
		//checks if all the required fields are completed
		$oldisbn = $_POST['oldisbn'];
		$isbn = $_POST['isbn'];
		$title = $_POST['title'];
		$author = $_POST['author'];
		$catid = $_POST['catid'];
		$price = $_POST['price'];
		$description = $_POST['description'];

		if(update_book($oldisbn, $isbn, $title, $author, $catid, $price, $description)) {
			//if all the fields have been completed, it prompts the user that the book has been updated successfully
			echo "<p>Book was updated.</p>";
		} else {
			//if one or more fields have not been completed, display error
			echo "<p>Book could not be updated.</p>";
		}
  	} else {
    	echo "<p>You have not filled out the form.  Please try again.</p>";
  	}
  	do_html_url("admin.php", "Back to administration menu"); //admin url
} else {
  	echo "<p>You are not authorised to view this page.</p>"; //if not logged in as admin, displays error message
}
do_html_footer();

?>