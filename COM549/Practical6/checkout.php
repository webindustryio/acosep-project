<?php

include('book_sc_fns.php');

session_start();
do_html_header("Checkout");
//Display the shopping cart
if(($_SESSION['cart']) && (array_count_values($_SESSION['cart']))) {
	//check if there are items added in the cart
	display_cart($_SESSION['cart'], false, 0);
	//displays the checkout form
	display_checkout_form();
} else {
	//Prompt the user that there are no items currently added in the car
	echo "<p>There are no items in your cart</p>";
}

display_button("show_cart.php", "continue-shopping", "Continue Shopping"); //show shopping button

do_html_footer();

?>