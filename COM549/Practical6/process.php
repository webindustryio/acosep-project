<?php  

include('book_sc_fns.php');

session_start();

do_html_header('Checkout');

$card_type = $_POST['card_type'];
$card_number = $_POST['card_number'];
$card_month = $_POST['card_month'];
$card_year = $_POST['card_year'];
$card_name = $_POST['card_name'];

if(($_SESSION['cart']) && ($card_type) && ($card_number) && ($card_month) && ($card_year) && ($card_name)) {
	//checks the card fields requirements
	display_cart($_SESSION['cart'], false, 0);

	display_shipping(calculate_shipping_cost()); //returns the calculated shipping cost

	if(process_card($_POST)) {
		//if the card details have been inserted it displayes a success message
		session_destroy(); //destroys the card details after the success
		echo "<p>Thank you for shopping with us. Your order has been placed.</p>";
		display_button("index.php", "continue-shopping", "Countinue Shopping");
	} else {
		//displays error if the cards was not valid
		echo "<p>Could not process your card. Please contact the card inssuer or try again.</p>";
		display_button("purchase.php", "back", "Back");
	}
} else {
	//displays error if the fields have not been completed
	echo "<p>You did not fill in all the fields, please try again.</p><hr />";
		display_button("purchase.php", "back", "Back");
}

do_html_footer();
?>