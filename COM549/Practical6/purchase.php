<?php  

include('book_sc_fns.php');
session_start();

do_html_header("Checkout");

$name = $_POST['name'];
$address = $_POST['address'];
$city = $_POST['city'];
$zip = $_POST['zip'];
$country = $_POST['country'];

if(($_SESSION['cart']) && ($name) && ($address) && ($city) && ($zip) && ($country)) {
	//checks if the cart as well as the required fields have been completed
	if(insert_order($_POST) != false) {
		display_cart($_SESSION['cart'], false, 0);

		display_shipping(calculate_shipping_cost()); //calculates the shipping cost, handeled in the books_fns.php

		display_card_form($name);
		display_button("show_cart.php", "continue-shopping", "Continue Shopping");
	} else {
		//prompts user with the error message
		echo "<p>Could not store data, please try again.</p>";
		display_button('checkout.php', 'back', 'Back');
	}
} else {
	//if the fields have not been completed, it prompts user with the error
	echo "<p>You did not fill in all the fields, please try again.</p><hr />";
	display_button('checkout.php', 'back', 'Back');
}

?>