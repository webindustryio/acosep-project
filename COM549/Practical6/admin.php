<?php  
	
	require_once('book_sc_fns.php');
	session_start();

	if(($_POST['username']) && ($_POST['passwd'])) {

		//checking if the admin is already logged in and has introduced the username and password
		$username = $_POST['username'];
		$passwd = $_POST['passwd'];

		if(login($username, $passwd)) {
			$_SESSION['admin_user'] = $username;
			//if the admin is already logged in, allow the user to the admin panel
		} else {
			//user has not logged in therefore the website displays a message to prompt the user to login
			do_html_header('Problem:');
			echo "<p>You could not be logged in.<br />
			You must be logged in to view this page.</p>";
			do_html_url('login.php', 'Login'); //url to the login page
			do_html_footer();
			exit;
		}
	}

	do_html_header("Administration");
	if(check_admin_user()) {
		display_admin_menu();
	} else {
		echo "<p>You are not authorized to enter the administration area.</p>";
	}

?>