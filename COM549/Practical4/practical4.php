<!DOCTYPE html>
<html>
<head>
<title>Youtube API</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/animate.css">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<style>
.display-4{
    margin-top:40px !important;
}
.video{
  width: 100% !important;
  height: 500px !important;
  background: url('img/loading.svg');
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-position: center; 
}
</style>
</head>
<?php


//Autoloader from Composer
if (!file_exists(__DIR__ . '/vendor/autoload.php')) {
  throw new \Exception('please run "composer require google/apiclient:~2.0" in "' . __DIR__ .'"');
}

require_once __DIR__ . '/vendor/autoload.php';

$htmlBody = <<<END
<form method="GET">
<div class="row">
  <div class="col-md-8">
  <div class="form-group">
    <label for="exampleInputEmail1">Search for a term</label>
    <input class="form-control" type="search" id="q" name="q" placeholder="Search...">
  </div>
</div>
  <div class="col-md-2">
  <div class="form-group">
    <label for="exampleInputEmail1">Max results</label>
    <input type="number" class="form-control" id="maxResults" name="maxResults" min="1" max="50" step="1" value="25">
  </div>
</div>
  <div class="col-md-2">
  <div class="form-group">
  <label for="exampleInputEmail1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
<button class="btn btn-danger btn-block" type="submit" value="Search">Search</button>
</div>
</div>
</div>
</br>
</form>  
END;

// This code will execute if the user entered a search query in the form
// and submitted the form. Otherwise, the page displays the form above.
if (isset($_GET['q']) && isset($_GET['maxResults'])) {
  //API Key
  $DEVELOPER_KEY = 'AIzaSyA0ZO0jQjNogzDWcQoYXT9T6pyb5zWP09Q'; 

  $client = new Google_Client();
  $client->setDeveloperKey($DEVELOPER_KEY);

  // Define an object that will be used to make all API requests.
  $youtube = new Google_Service_YouTube($client);

  $htmlBody = '';
  try {

    // Call the search.list method to retrieve results matching the specified
    // query term.
    
    $searchResponse = $youtube->search->listSearch('id,snippet', array(
      'q' => $_GET['q'],
      'maxResults' => $_GET['maxResults'] +1,
    ));

    $videos = '';
    $channels = '';
    $playlists = '';

    // Add each result to the appropriate list, and then display the lists of
    // matching videos, channels, and playlists.
    foreach ($searchResponse['items'] as $searchResult) {
      switch ($searchResult['id']['kind']) {
        case 'youtube#video':
          $videos .= sprintf('<div class="container"><div class="row" class="animated fadeIn" style="border: 1px solid black; padding:15px; margin-top:15px;">
          <div class="col col-md-2">
          <img class="fluid" src="%s">
          </div>
          <div class="col col-md-10">
          <h3 data-toggle="modal" data-target="#a'.$searchResult['id']['videoId'].'">%s</h3>
          <p>%s</p>
          </div>
          <div class="modal fade bd-example-modal-lg" id="a'.$searchResult['id']['videoId'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="'.$searchResult['id']['videoId'].'">'.$searchResult['snippet']['title'].'</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <iframe role="status"" height="800" class="video" src="https://www.youtube.com/embed/'.$searchResult['id']['videoId'].'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>
          </div></div>', 'http://img.youtube.com/vi/'.$searchResult['id']['videoId'].'/default.jpg', 
          $searchResult['snippet']['title'], $searchResult['snippet']['description'], $searchResult['id']['videoId']);
          
          break;
        case 'youtube#channel':
          $channels .= sprintf('<li>%s (%s)</li>',  
              $searchResult['snippet']['title'], $searchResult['id']['channelId']);
          break;
        case 'youtube#playlist':
          $playlists .= sprintf('<li>%s (%s)</li>',
              $searchResult['snippet']['title'], $searchResult['id']['playlistId']);
          break;
      }
    }

    $htmlBody .= <<<END
    <h3>Videos</h3>
    <div>$videos</div>
END;
  } catch (Google_Service_Exception $e) {
    $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  } catch (Google_Exception $e) {
    $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
      htmlspecialchars($e->getMessage()));
  }
}

?>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Web Applications Development</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Projects
        </a>
        <div class="dropdown-menu"  aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical1/practical1.php">1. Currency Converter</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical2/practical2.html">2. Registration form</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical3/practical3.php">3. Retrieving Weather Information</a>
          <a class="dropdown-item active" href="http://webindustry.io/COM549/Practical4/practical4.php">4. Youtube Data API Integration</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical5/practical5.php">5. Guardian news API</a>
          <a class="dropdown-item" href="http://webindustry.io/COM549/Practical6/">6. Shopping Cart</a>
        </div>
      </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="jumbotron jumbotron-fluid text-center">
  <div class="container">
  <div style="align:inline"><img class="display-4 img-fluid animated fadeInLeft" width="300px" src="img/YouTube.png"><img class="display-4 img-fluid animated fadeInRight" width="250px" src="img/Ulster-University.png"></div>
    <p class="lead animated fadeIn">PHP Youtube Application- Practical 4</p>
</div>
  </div>
<div class="container">
  <div><?php echo $htmlBody ?></div>
</div>
</br>
</div>
</body>
</html>




