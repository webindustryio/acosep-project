<!DOCTYPE html>
<html>
<head>
<title>Registration form</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/animate.css">
<script src="js/bootstrap.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<style>
.display-4{
    margin-top:40px !important;
}
</style>
</head>
<?php
//Database credentials
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'practical2');
define('DB_PASSWORD', 'Ulster2020&');
define('DB_NAME', 'practical2');
 
/* Attempt to connect to MySQL database */
$link = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);
 
// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
} else {
}

$sql = "SELECT Firstname, Surname, Username, DateRegistered, UserType FROM Users ORDER BY DateRegistered DESC";
//SQL query displaying the most recent users at the top
$result = $link->query($sql);

?>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">Web Applications Development</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Projects
        </a>
        <div class="dropdown-menu"  aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">1. Currency Converter</a>
          <a class="dropdown-item active" href="#">2. Registration form</a>
          <a class="dropdown-item" href="#">3. Retrieving Weather Information</a>
          <a class="dropdown-item" href="#">4. Youtube Data API Integration</a>
          <a class="dropdown-item" href="#">5. Guardian news API</a>
          <a class="dropdown-item" href="#">6. Shopping Cart</a>
        </div>
      </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="jumbotron jumbotron-fluid text-center">
  <div class="container">
    <img class="display-4 img-fluid animated fadeIn" width="300px" src="img/Ulster-University.png">
    <h1 class="display-4">DASHBOARD</h1>
    <p class="lead">Users Table</p>
</div>
  </div>
<div class="container">
<table class="table table-bordered animated bounceIn">
  <thead>
    <tr>
      <th scope="col">FirstName</th>
      <th scope="col">Surname</th>
      <th scope="col">Username</th>
      <th scope="col">Sign up date</th>
      <th scope="col">User Type</th>
    </tr>
  </thead>
  <tbody>
<?php
  if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo '<tr>';
        echo '<td>'. $row["Firstname"].'</td><td>'. $row["Surname"].'</td><td>'. $row["Username"].'</td><td>'. $row["DateRegistered"].'</td><td>'. $row["UserType"].'</td>';
        echo '</td>';
    }
} else {
    echo "0 results";
}
    ?>
  </tbody>
</table>
</div>
</body>
</html>



