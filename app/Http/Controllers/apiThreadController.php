<?php

namespace App\Http\Controllers;

use App\Thread;
use App\Channel;
use App\User;
use Illuminate\Http\Request;

class apiThreadController extends Controller
{
    
    /* public function index()
    {
        //
        $threads = Thread::latest()->with('owner', 'channel')->get();
        return response()->json($threads);
    }
 */
    public function index(Channel $channel)
    {

        if($channel->exists){
            $threads = $channel->threads()->latest()->with('owner','channel')->get();
           // $channelId = Channel::where('slug', $channelSlug)->first()->id;
           // $threads = Thread::where('channel_id', $channelId)->latest()->get();
        } else {
            $threads = Thread::latest()->with('owner','channel')->get();
        }
        

        //get all the threads in desc order
     //   $threads = Thread::latest()->get();
        return response()->json($threads);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request->all());

        
        $thread = Thread::create([
           'user_id' => auth()->user()->id,
           'channel_id' => request('channel_id'),
           'title' => request('title'),
           'body' => request('body')
        ]);


        return response()->json($thread);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show($channelId, $id)
    {
        //
       // return view('threads.show', compact('thread'));
       //$thread->with('replies')->get();
       $thread = Thread::with('replies', 'owner')->find($id);
       return response()->json($thread);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function edit(Thread $thread)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thread $thread)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy(Thread $thread)
    {
        //
    }

    public function details(Request $request){
        return $request->user();
    }
}
