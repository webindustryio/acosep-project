<?php

namespace App\Http\Controllers;

use App\Thread;
use App\Channel;
use App\User;
use Illuminate\Http\Request;

class ThreadController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth')->only('create', 'store'); 
        //check if user authentifated only for the create and store functions
        //thus users will be redirected to login
        //middleware used in the guest_may_not_post_threads test
    }

    public function index(Channel $channel)
    {

        if($channel->exists){
            $threads = $channel->threads()->latest()->get();
           // $channelId = Channel::where('slug', $channelSlug)->first()->id;
           // $threads = Thread::where('channel_id', $channelId)->latest()->get();
        } else {
            $threads = Thread::latest()->get();
        }

        //get all the threads in desc order
     //   $threads = Thread::latest()->get();
        return view('threads.index', compact('threads'));
    }

    /**\
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('threads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //dd($request->all());
      $this->validate(request(), [
            'title' => 'required',
            'body' => 'required',
            'channel_id' => 'required|exists:channels,id',
            ]);
/*

        $thread = Thread::create([
           'user_id' => auth()->id(),
           'channel_id' => ('channel_id'),
           'title' => request('title'),
           'body' => request('body')
        ]);
 */
        $thread = Thread::create([
            'user_id' => auth()->user()->id,
            'channel_id' => request('channel_id'),
            'title' => request('title'),
            'body' => request('body')
         ]);

        return redirect($thread->path()); 

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show($channelId, Thread $thread)
    {
        //
       return view('threads.show', compact('thread'));
       //$thread->with('replies')->get();
       //$thread = Thread::with('replies', 'owner')->find($thread);
       //return response()->json($thread);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function edit(Thread $thread)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thread $thread)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy(Thread $thread)
    {
        //
    }

    
}
