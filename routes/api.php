<?php

use Illuminate\Http\Request;
use App\Thread;

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Auth\AuthController@login')->name('login');
    Route::post('register', 'Auth\AuthController@register');
    
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        route::get('/details', 'apiThreadController@details');
        Route::get('/threads', 'apiThreadController@index');
        Route::get('/threads/create', 'apiThreadController@create');
        Route::get('/threads/{channel}', 'apiThreadController@index');
        Route::get('/threads/{channel}/{thread}', 'apiThreadController@show');
        Route::post('/threads', 'apiThreadController@store');
        Route::post('/threads/{channel}/{thread}/replies', 'apiReplyController@store');
    });
});